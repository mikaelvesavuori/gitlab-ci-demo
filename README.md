# GitLab CI/CD demo through REST API

This is a basic demonstration of what is needed to run a GitLab CI build by calling their REST API.

We are using Figmagic for this demo to demonstrate running it in CI. Even if you don't care for that specific tool, you'll get the gist of how to automate stuff in CI.

## Instructions

Clone this repo and put it on GitLab.

Set secrets for the pipeline as per:

- `FIGMA_TOKEN` as your Figma API token
- `FIGMA_URL` to point to your Figma file ID

### Authentication

You will have to use pipeline trigger tokens for this to work. Navigate, using the web interface, to your repository. Go to `Settings > CI/CD` and you should find the possibility to create a token if you scroll down a bit.

## Example call to API

Send a payload as per:

```json
POST https://gitlab.com/api/v4/projects/{PROJECT_ID}/trigger/pipeline?token={TOKEN}&ref={REF}
```

## References

- [GitLab: Trigger pipelines by using the API](https://docs.gitlab.com/ee/ci/triggers/)
